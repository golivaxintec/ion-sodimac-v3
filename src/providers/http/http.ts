/* VER 0.1 */ 
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PersistenceProvider } from '../persistence/persistence';
import { HttpOptions } from './httpOptions';
import { CacheProvider } from '../persistence/cache';

@Injectable()
export class HttpProvider {

  private urlServer;
  constructor(private http: HttpClient, private cache: CacheProvider, private persistence: PersistenceProvider) {
    this.urlServer = "";
  }

  makeCall(options : HttpOptions){
    return new Promise<any>(async(resolve, reject) => {
      try{
        // Check if need cache
        if (options.cache) {
          let cacheData = await this.cache.getCache(options.url);
          if(cacheData != null){ 
            let response = cacheData;
            response.internalCache = true;
            return resolve(cacheData); 
          }
        }
        let headers = new HttpHeaders();
        // Add headers
        let optionsHeaders = options.getHeaders();
        if (optionsHeaders.length > 0){
          optionsHeaders.forEach((item =>{
            headers = headers.append(item.key, item.value);
          }));
        }else{
          headers = headers.append('Content-Type', 'application/json');
        }
        // Add Auth header
        if(options.auth){
          let token = await this.persistence.getData('authToken');
          if (token != null) { headers = headers.append('Authorization', 'Basic ' + token); }
        }
        // create call options
        let req;
        
        if(options.method == "GET"){
          req = this.http.get(this.urlServer + options.url);
        }
        else if (options.method == "POST"){
          let body = {};
          if (options.bodyData != undefined) {
            body = options.bodyData;
          }
          req = this.http.post(options.url,body,{headers:headers});
        }
        req.subscribe(async res => {
          let response = res;
          response.internalCache = false;
          if(options.cache){
            await this.cache.setCache(options.url,response,options.cacheTime);
          }
          return resolve(response);
          },error => {
            console.error(error);
            return reject(); 
          });
      }catch(e){
        console.error(e);
        return reject(e);
      }
    });
  }

}
