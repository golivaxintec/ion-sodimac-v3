/* VER 0.1 */
import { Injectable } from '@angular/core';
import { UserProvider } from '../user/user';
import { HttpProvider } from './http';
import { ConfigProvider } from '../../config/config';
import { HttpOptions } from './httpOptions';
import { PersistenceProvider } from '../persistence/persistence';

@Injectable()
export class clientProvider {

    private appName: string;
    private url: string;
    private country : string;

    constructor(private config: ConfigProvider, private http: HttpProvider){
        this.appName = this.config.getValue('appName');
        this.url = this.config.getValue('clientEndpoint');
        this.country = this.config.getValue('country');
    }

    /* FAVORITOS */

    async setFavorite(sku: string){
        const urlRequest = this.url + '/favorites/' + this.appName + '/' + this.country;
        const options = new HttpOptions(urlRequest, "POST",true);
        options.bodyData = { sku: sku };
        return await this.http.makeCall(options);
    }

    async getFavorites(){

    }

    async deleteFavorite(){

    }

     /* CARRO */



}