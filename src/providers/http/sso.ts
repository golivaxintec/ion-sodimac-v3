/* VER 0.1 */
import { Injectable } from '@angular/core';
import { UserProvider } from '../user/user';
import { HttpProvider } from './http';
import { ConfigProvider } from '../../config/config';
import { HttpOptions } from './httpOptions';
import { PersistenceProvider } from '../persistence/persistence';

@Injectable()
export class ssoProvider {
    url : string;
    constructor(private user: UserProvider, private http: HttpProvider, private config: ConfigProvider,
         private persistence: PersistenceProvider){
         this.url = this.config.getValue('ssoEndpoint');
    }

    async login(email: string, password:string){
       try{
           const clientID = this.config.getValue('clientID');
           const urlRequest = this.url + '/Oauth2/v2/token?type=ATG&appId=' + clientID;
           const options = new HttpOptions(urlRequest, "POST");
           options.bodyData = 'grant_type=password&scope=profile';
           options.addHeader('Authorization', 'Basic ' + email + ":" + password);
           options.addHeader('Content-Type','application/x-www-form-urlencoded');
           options.addHeader('Accept', 'aplication/json');
           let requestResponce = await this.http.makeCall(options);
           if (requestResponce.access_token.length > 20){
                await this.persistence.setData('authToken', requestResponce.access_token);
                return true;
           }
           return false;
       }catch(e){
           return false;
       }
    }

    async register(email: string, country: string, nationalId: string, name: string, lastName: string,
        birthDay: number, birthMonth: number, birthYear: number, gender: string, phone: string
        , password: string, notification_check : boolean){
        try{
            
            const urlRequest = this.url + '/Oauth2/api/atg_register';
            const options = new HttpOptions(urlRequest, "POST");
            options.bodyData({
                country: 'CL', nationalId: nationalId, name: name, lastName: lastName, birthDay: birthDay, birthMonth: birthMonth,
                birthYear: birthYear, gender: gender, phone: phone, email: email, password: password, notification_check: notification_check
            });
            let requestResponce = await this.http.makeCall(options);
            if (requestResponce.success === true) {
                await this.persistence.setData('authToken', requestResponce.token);
                return true;
            }
            return false;
        }catch(e){
            return false;
        }
    }

    async recovery(email: string){
        try{
            const clientID = this.config.getValue('clientID');
            const urlRequest = this.url + '/Oauth/api/recovery';
            const options = new HttpOptions(urlRequest, "POST");
            options.bodyData={
                country: 'CL', client_id: clientID, email: email
            };
            let requestResponce = await this.http.makeCall(options);
            return requestResponce.success;
        }catch(e){
            return false;
        }
    }

    async getProfile(){
        return this.user.getProfile();
    }

}