/* VER 0.1 */
import { Injectable } from '@angular/core';
@Injectable()
export class HttpOptions {

    constructor(public url: string, public method:string = 'GET', 
        public auth: boolean = false, public cache: boolean = false, public cacheTime: number = 300000) { // 300000 = 5 minutes
        this.url = url;
        this.method = method;
        this.auth = auth;
        this.cache = cache;
        this.cacheTime = cacheTime;
    }
    public bodyData = undefined;
    body(bodyData: object){
        this.bodyData = bodyData;
    }
    private headers = [];
    addHeader(key: string, value: string){
        this.headers.push({key:key,value:value});
    }
    public contentType = null;
    setContentType(contentType:string){
        contentType = contentType;
    }

    getHeaders(){   
        return this.headers;
    }
}