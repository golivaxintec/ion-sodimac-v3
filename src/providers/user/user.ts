/* VER 0.1 */
import { Injectable } from '@angular/core';
import { PersistenceProvider } from '../persistence/persistence';
import { UserProfile } from './userProfile';

@Injectable()
export class UserProvider {

  constructor(private persistence: PersistenceProvider) {
  }


  async setUser(jwt: string){
    try{
      const userPayloadBase64 = atob(jwt.split('.')[2]);
      let payloadObject = JSON.parse(userPayloadBase64);
      await this.persistence.setData('userPayloadData',payloadObject);
      return true;
    }catch(e){
      return false;
    }
  }

  async getProfile(){
    try{
      return <UserProfile>await this.persistence.getData('userPayloadData');  
    }catch(e){
      return new UserProfile();
    }
    
  }
  

}
