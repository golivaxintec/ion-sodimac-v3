/* VER 0.1 */
import { Injectable } from '@angular/core';
@Injectable()
export class UserProfile {
    
    public primarysid: string;
    public unique_name: string;
    public email: string;
    public application: string;
    public nationalId: string;
    public country: string;
    public groupsid: string;
    public scopes: string;
    public aud: string;
    public iss: string;
    public exp: number;
    public nbf: number;
    public scope: Array<String>;
    public role: Array<String>;

}
