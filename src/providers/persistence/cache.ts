/* VER 0.1 */
import { Injectable } from "@angular/core";
import { PersistenceProvider } from "./persistence";
@Injectable()
export class CacheProvider {

    constructor(private persistence: PersistenceProvider){
    }

    async getCache(key:string){
        try{
            let dataCache = await <any> this.persistence.getData(key);
            if(dataCache == null){ return null; } // key dont have data
            let actualTimeStamp = Math.floor(Date.now() / 1000);
            // tts time exceeded
            if (actualTimeStamp - dataCache.timeStamp > dataCache.tts){
                return null;
            }else{ // return from Cache
                return dataCache.data
            }
        }catch(e){
            console.log(e);
            return null;
        }
    }

    async setCache(key:string, data:Object , tts:Number=(60*1000*15)){
        try{
            let timeStamp = Math.floor(Date.now() / 1000);
            let cacheObject = {
                data : data,
                timeStamp: timeStamp,
                tts : tts
            }
            await this.persistence.setData(key, cacheObject);
            return true;
        }catch(e){
            return false;
        }
    }

}