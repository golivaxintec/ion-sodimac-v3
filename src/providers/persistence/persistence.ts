/* VER 0.1 */
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class PersistenceProvider {

  constructor(private storage: Storage) {
  }

  getData(key : string) {
    return this.storage.get(key);
  }

  setData(key:string, value:Object) {
    return this.storage.set(key, value);
  }

}
