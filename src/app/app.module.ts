import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

/* PAGES */

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

/* CUSTOM PROVIDERS */

/* PERSISTENCE */
import { PersistenceProvider } from '../providers/persistence/persistence';
import { CacheProvider } from '../providers/persistence/cache';
/* HTTP */ 
import { HttpProvider } from '../providers/http/http';
import { HttpOptions } from '../providers/http/httpOptions';
import { carProvider } from '../providers/http/car';
import { clientProvider } from '../providers/http/client';
import { ssoProvider } from '../providers/http/sso';
/* USER */
import { UserProvider } from '../providers/user/user';
import { UserProfile } from '../providers/user/userProfile';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    /* CUSTOM PROVIDERS */
    PersistenceProvider, CacheProvider,
    HttpProvider, HttpOptions, carProvider, clientProvider, ssoProvider,
    UserProvider, UserProfile
  ]
})
export class AppModule {}
