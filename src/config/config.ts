import { Injectable } from '@angular/core';
import { devVariables } from './development';
import { prodVariables } from './production';

declare const process: any;
@Injectable()
export class ConfigProvider {

    variables: any;
    constructor(){       
        if (process.env.IONIC_ENV === 'prod') {
            this.variables = devVariables;
        } else{
            this.variables = prodVariables;
        }
    }

    getValue(key: string){
        try{
            return this.variables[key];
        }catch(e){
            return null;
        }
    }

    isDev(){
        return process.env.IONIC_ENV != 'prod';
    }
}